.PHONY: all
all: get_gpu_info saxpy saxpy_multi_gpu

get_gpu_info: get_gpu_info.cu
	nvcc get_gpu_info.cu -o get_gpu_info

saxpy: saxpy.cu
	nvcc saxpy.cu -O3 -o saxpy

saxpy_multi_gpu: saxpy_multi_gpu.cu
	nvcc saxpy_multi_gpu.cu -O3 -o saxpy_multi_gpu
