#include <iostream>

int main() {
    int n_gpus;
    cudaGetDeviceCount(&n_gpus);

    if (n_gpus == 0) {
        std::cout << "Found no NVIDIA GPUs.\n";
        return 0;
    }

    if (n_gpus == 1) {
        std::cout << "Found 1 NVIDIA GPU.\n\n";
    }

    if (n_gpus > 1) {
        std::cout << "Lucky you, you have " << n_gpus << " GPUs.\n\n";
    }

    for (int i = 0; i < n_gpus; ++i) {
        // This struct has many other fields too.
        // https://docs.nvidia.com/cuda/cuda-runtime-api/structcudaDeviceProp.html
        cudaDeviceProp prop;
        cudaGetDeviceProperties(&prop, i);

        std::cout << "GPU " << i << ":\n";
        std::cout << "    name: " << prop.name << '\n';
        std::cout << "    compute capability: " << prop.major << '.' << prop.minor << '\n';
        std::cout << "    global memory: " << prop.totalGlobalMem << " bytes\n";
        std::cout << "    constant memory: " << prop.totalConstMem << " bytes\n";
        std::cout << "    L2 cache: " << prop.l2CacheSize << " bytes\n";
        std::cout << "    shared memory per block: " << prop.sharedMemPerBlock << " bytes\n";
        std::cout << "    shared memory per multiprocessor: " << prop.sharedMemPerMultiprocessor << " bytes\n";
        std::cout << "    registers per block: " << prop.regsPerBlock << '\n';
        std::cout << "    registers per multiprocessor: " << prop.regsPerMultiprocessor << '\n';
        std::cout << "    multiprocessor count: " << prop.multiProcessorCount << '\n';
        std::cout << "    max threads per multiprocessor: " << prop.maxThreadsPerMultiProcessor << '\n';
        std::cout << "    max grid size: " << prop.maxGridSize[0] << " x "
                                           << prop.maxGridSize[1] << " x "
                                           << prop.maxGridSize[2] << '\n';
        std::cout << "    max threads dim: " << prop.maxThreadsDim[0] << " x "
                                             << prop.maxThreadsDim[1] << " x "
                                             << prop.maxThreadsDim[2] << '\n';
        std::cout << "    max threads per block: " << prop.maxThreadsPerBlock << '\n';
        std::cout << '\n';
    }

    return 0;
}
